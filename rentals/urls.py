"""rentals URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from movie_rental import apiviews

admin.site.site_header = 'Rentals Administration'
admin.site.site_title = 'Rentals | Administration'
admin.site.index_title = 'Welcome to Rentals Portal'

router = routers.DefaultRouter()
router.register(r'clients', apiviews.ClientViewSet)
router.register(r'movies', apiviews.MovieViewset)
router.register(r'rentedMovies', apiviews.RentalViewSet)

urlpatterns = [
    path('', include('movie_rental.urls')),
    path('api/', include(router.urls)),
    path('admin/', admin.site.urls),
]
