from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.dispatch import receiver
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from .manager import CustomUserManager
from .sendMail import SendMail

sendmail = SendMail()

# Create your models here.

MEMBERSHIPTYPES = (
    (1, "Pay as you go"),
    (2, "Monthly"),
    (3, "Quaterly"),
    (4, "Annually")
)

STATUSES = (
    (1, "Single"),
    (2, "Married"),
    (3, "Divorced"),
    (4, "Widowed"),
)

RATINGS = (
    (1, "PG"),
    (2, "10+"),
    (3, "13+"),
    (4, "16+"),
    (5, "18+"),
)

MOVIE_STATUSES = (
    (1, "Not returned"),
    (2, "Returned")
)

GENRES = (
    ("​Action", "​Action"),
    ("Animation", "Animation"),
    ("Comedy", "Comedy"),
    ("Crime", "Crime"),
    ("Drama", "Drama"),
    ("Fantasy", "Fantasy"),
    ("Historical", "Historical"),
    ("Horror", "Horror"),
    ("Romance", "Romance"),
    ("Science Fiction", "Science Fiction"),
    ("Thriller", "Thriller"),
    ("Western", "Western"),
    ("Other", "Other")
)

if settings.DEBUG:
    url = 'http://127.0.0.1:8080/'
else:
    url = 'https' + settings.ALLOWED_HOSTS[0] + '/'


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField('Email Address', unique=True)
    first_name = models.CharField(null=False, max_length=100)
    last_name = models.CharField(null=False, max_length=100)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    objects = CustomUserManager()

    class Meta:
        db_table = 'user'
        verbose_name = 'User'

    def __str__(self):
        return self.first_name


@receiver(post_save, sender=CustomUser)
def confirmation_mail(sender, instance, **kwargs):
    if kwargs['created']:
        if not instance.is_superuser:
            subject, to = 'New account', instance.email
            html_content = '<html>' \
                '<body>' \
                '<p>Dear ' + instance.first_name + ', </p>' \
                '<p>Your account has been successfully created.</p>' \
                '<p>You can login and set your password by clicking below.</p>' \
                '<p><a href="' + url + '">Click Here</a></p>' \
                '<p><b>Thank you</b></p>' \
                '</body>' \
                '</html>'

            sendmail.send(subject, to, html_content)


class UserProfile(models.Model):
    address = models.TextField(null=False)
    telephone_number = models.CharField(null=False, max_length=20)
    date_of_birth = models.DateField(null=False)
    hire_date = models.DateField(null=False)
    marital_status = models.SmallIntegerField(choices=STATUSES, default=1)
    create_date = models.DateTimeField(auto_now_add=True, editable=False)
    user = models.OneToOneField(
        CustomUser, on_delete=models.PROTECT, db_column='user_id')

    class Meta:
        db_table = 'user_profile'


class Client(models.Model):
    id = models.AutoField(primary_key=True, editable=False)
    first_name = models.CharField(null=False, max_length=100)
    last_name = models.CharField(null=False, max_length=100)
    email_address = models.EmailField(null=False, unique=True)
    address = models.TextField(null=False)
    telephone_number = models.CharField(null=False, max_length=20)
    date_of_birth = models.DateField(null=False)
    membership_type = models.SmallIntegerField(
        choices=MEMBERSHIPTYPES, default=1)
    create_date = models.DateTimeField(auto_now_add=True, editable=False)
    user = models.ForeignKey(
        CustomUser, on_delete=models.PROTECT, db_column='created_by')

    class Meta:
        db_table = 'client'

    def __str__(self):
        return self.first_name + " " + self.last_name


class Movie(models.Model):
    id = models.AutoField(primary_key=True, editable=False)
    title = models.CharField(null=False, max_length=200, unique=True)
    director = models.CharField(null=False, max_length=100)
    rating = models.SmallIntegerField(choices=RATINGS, default=-1)
    genre = models.CharField(null=False, choices=GENRES,
                             max_length=20, default="Other")
    description = models.TextField()
    dailyRentalRate = models.FloatField('Daily Rental Rate', null=False)
    numberInStock = models.IntegerField('Number in Stock', null=False)
    create_date = models.DateTimeField(auto_now_add=True, editable=False)
    user = models.ForeignKey(
        CustomUser, on_delete=models.PROTECT, db_column='created_by')

    class Meta:
        db_table = 'movie'

    def __str__(self):
        return self.title


class Rentals(models.Model):
    id = models.AutoField(primary_key=True, editable=False)
    movie = models.ForeignKey(Movie, on_delete=models.PROTECT)
    client = models.ForeignKey(Client, on_delete=models.PROTECT)
    quantity = models.SmallIntegerField(null=False)
    status = models.SmallIntegerField(choices=MOVIE_STATUSES, default=1)
    date_rented = models.DateField(null=False, auto_now_add=True)
    date_returned = models.DateField(null=True)
    rentalAmount = models.FloatField(null=False, default=0, editable=False)
    shop_attendant = models.ForeignKey(CustomUser, on_delete=models.PROTECT)
    create_date = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        db_table = 'rentals'

    def __str__(self):
        return str(self.id)


class Login_Log(models.Model):
    shop_attendant = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    login_date = models.DateTimeField()
    logout_date = models.DateTimeField(null=True)

    class Meta:
        db_table = 'login_log'


class History(models.Model):
    action = models.CharField(blank=True, max_length=20)
    model = models.CharField(blank=True, max_length=50)
    create_date = models.DateTimeField(auto_now_add=True, editable=False)
    user = models.ForeignKey(
        CustomUser, on_delete=models.PROTECT, db_column='done_by')

    class Meta:
        db_table = 'history'
