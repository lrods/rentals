from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.contrib import messages
from django.core import serializers
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.hashers import make_password, check_password
from django.contrib.auth.password_validation import validate_password, ValidationError
from django.utils import timezone
from django.utils.timezone import localtime, now
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from .models import CustomUser, Client, Movie, Login_Log, Rentals, RATINGS
from datetime import datetime
import json
from .serializers import ClientDetailsSerializer
from .sendMail import SendMail
from sentry_sdk import capture_message
from django.conf import settings

# Create your views here.

sendmail = SendMail()

if settings.DEBUG:
    url = 'http://127.0.0.1:8080/'
else:
    url = 'https' + settings.ALLOWED_HOSTS[0] + '/'


def Login(request):
    request.session.flush()
    return render(request, "movie_rental/login.html")


def Signout(request):
    try:
        last_login_detail = Login_Log.objects.filter(
            shop_attendant_id=request.user.id).latest("id")
        last_login_detail.logout_date = timezone.now()
        last_login_detail.save()
        logout(request)
        request.session.flush()
        return redirect("login")
    except Exception:
        return redirect("login")


def Signin(request):
    if request.method == "POST":
        email = request.POST.get("email")
        password = request.POST.get("password")
        user = authenticate(email=email, password=password)

        if user is not None:
            if user.last_login is None:
                return redirect(reverse('newpassword') + '?email=' + user.email)

            if user.is_active:
                login(request, user)

                try:
                    llog = Login_Log(login_date=timezone.now(
                    ), logout_date=None, shop_attendant_id=user.id)
                    llog.save()
                except Exception:
                    pass

                return redirect("home")
            else:
                messages.error(request, "Account desactivated",
                               extra_tags="alert")
                return redirect("login")
        else:
            messages.error(request, "Invalid login details",
                           extra_tags="alert")
            capture_message('Unsuccessful login attempt!', level='warning')
            return redirect("login")
    else:
        return redirect("login")


def NewPassword(request):
    if request.method == "POST":
        email = request.POST.get('mail')
        new_password = request.POST.get("password")

        if email is None:
            return redirect("login")
        else:
            user = get_object_or_404(CustomUser, email=email)

        try:
            validate_password(new_password)

            if check_password(new_password, user.password):
                messages.error(
                    request, "You have already used this password", extra_tags="alert")
                return redirect(reverse('newpassword') + '?email=' + email)
            else:
                try:
                    with transaction.atomic():
                        user.password = make_password(new_password)
                        llog = Login_Log(login_date=timezone.now(
                        ), logout_date=None, shop_attendant_id=user.id)
                        user.save()
                        llog.save()
                except Exception as ex:
                    messages.error(
                        request, "Unable to save password. Please try again", extra_tags="alert")
                    capture_message(ex, level='error')
                    return redirect(reverse('newpassword') + '?email=' + email)
                else:
                    html_content = '<html>' \
                        '<body>' \
                        '<p>Hello, </p>' \
                        '<p>Your account password has changed.</p>' \
                        '<p>If you didn\'t request this password change please contact the <a href="demo.apps19@gmail.com">Administrator</a>.</p>' \
                        '<p><b>Thank you</b></p>' \
                        '</body>' \
                        '</html>'
                    sendmail.send('Your password was reset',
                                  'demo.apps19@gmail.com', html_content)
                    login(request, user)
                    return redirect('home')

        except ValidationError as ve:
            messages.error(request, ve.messages[0], extra_tags="alert")
            return redirect(reverse('newpassword') + '?email=' + email)
    else:
        return render(request, "movie_rental/newpassword.html", {"mail": request.GET.get('email')})


def ResetPassword(request):
    if request.method == "POST":
        email_address = request.POST.get("email")

        subject, to = 'Password reset', email_address
        html_content = '<html>' \
            '<body>' \
            '<p>Forgot your password? No problem! Click on the link below or</p>' \
            '<p>copy and paste the address in your browser to reset.</p>' \
            '<p>Reset link: ' + url + 'newpassword/?email=' + email_address + ' </p>' \
            '<p><b>Thank you</b></p>' \
            '</body>' \
            '</html>'

        if sendmail.send(subject, to, html_content):
            messages.success(
                request, "The reset link has been sent. Please check your mailbox")
            return redirect('forgot')
        else:
            messages.error(
                request, "Unable to send the reset link. Please try again")
            return redirect('forgot')
    else:
        return render(request, "movie_rental/resetpassword.html")


@login_required(login_url='/')
def Home(request):
    return render(request, "movie_rental/index.html", {"User": request.user.first_name})


@login_required(login_url='/')
def ClientView(request):
    if request.method == "POST":
        first_name = request.POST.get("fname")
        last_name = request.POST.get("lname")
        email_address = request.POST.get("email_address")
        address = request.POST.get("address")
        telephone_number = request.POST.get("tel_number")
        date_of_birth = request.POST.get("date_of_birth")
        membership_type = request.POST.get("member_type")
        client_id = request.POST.get("client_id")

        if client_id:
            client = get_object_or_404(Client, pk=int(client_id))

            try:
                client.email_address = email_address
                client.address = address
                client.telephone_number = telephone_number
                client.membership_type = int(membership_type)
                client.save()

                messages.success(
                    request, "The client details was successfully updated", extra_tags="alert")
                return redirect("client")
            except Exception as ex:
                messages.error(
                    request, "Unable to update client details. Please try again", extra_tags="alert")
                capture_message(ex, level='error')
                return redirect('addClient')
        else:
            try:
                client = Client(first_name=first_name, last_name=last_name, email_address=email_address, address=address,
                                telephone_number=telephone_number, date_of_birth=date_of_birth, membership_type=int(membership_type), user=request.user)

                client.save()

                if client.id:
                    messages.success(
                        request, "New client successfully added", extra_tags="alert")
                    return redirect("client")
            except Exception as ex:
                messages.error(
                    request, "Unable to add new client. Please try again", extra_tags="alert")
                capture_message(ex, level='error')
                return redirect('addClient')
    else:
        return render(request, "movie_rental/clients.html", {"User": request.user.first_name})


@login_required(login_url='/')
def AddNewClient(request):
    return render(request, "movie_rental/client.html", {"User": request.user.first_name})


@login_required(login_url='/')
def UpdateClient(request, id):
    client = get_object_or_404(Client, pk=id)
    data = {"client_id": id, "fname": client.first_name, "lname": client.last_name, "email_address": client.email_address,
            "tel_number": client.telephone_number, "address": client.address, "date_of_birth": datetime.strftime(client.date_of_birth, "%Y-%m-%d"),
            "member_type": client.membership_type, "User": request.user.first_name}
    return render(request, "movie_rental/client.html", data)


def ClientDetails(request, id):
    client = get_object_or_404(Client, pk=id)
    serializer = ClientDetailsSerializer(client)
    return JsonResponse(serializer.data)


@login_required(login_url='/')
def MovieList(request):
    return render(request, "movie_rental/movies.html", {"User": request.user.first_name})


@login_required(login_url='/')
def RentMovie(request, id):
    movie = get_object_or_404(Movie, pk=id)

    if request.method == "GET":
        clients = Client.objects.only("id", "first_name", "last_name")

        data = {"client_id": id, "movie_id": movie.id, "movie_title": movie.title, "genre": movie.genre,
                "rating": get_rating_value(movie.rating), "clients": clients, "User": request.user.first_name}

        return render(request, "movie_rental/rentmovie.html", data)
    else:
        client_id = request.POST.get("client")
        quantity = request.POST.get("quantity")

        rental = Rentals(movie=movie, client=get_object_or_404(
            Client, id=int(client_id)), quantity=int(quantity), shop_attendant=request.user)

        movie.numberInStock = movie.numberInStock - int(quantity)

        try:
            with transaction.atomic():
                rental.save()
                movie.save()

                if rental.id:
                    messages.success(
                        request, "Successfull transaction", extra_tags="alert")
                    return redirect("movies")
        except Exception as ex:
            messages.error(
                request, "Unable to complete transaction. Please try again", extra_tags="alert")
            capture_message(ex, level='error')
            return render(request, "movie_rental/rentmovie.html", {"User": request.user.first_name})


@login_required(login_url='/')
def RentedMovies(request):
    return render(request, "movie_rental/returns.html", {"User": request.user.first_name})


@login_required(login_url='/')
def ReturnMovie(request, rentalId, movieId):
    rental = get_object_or_404(Rentals, pk=rentalId)
    movie = get_object_or_404(Movie, pk=movieId)

    return_date = localtime(now()).date()
    nod = get_number_of_days(rental.date_rented, return_date)
    rental_amount = ((movie.dailyRentalRate * rental.quantity) if nod ==
                     0 or nod == 1 else (nod * movie.dailyRentalRate * rental.quantity))

    try:
        with transaction.atomic():
            rental.status = 2
            rental.date_returned = return_date
            rental.rentalAmount = rental_amount
            movie.numberInStock = movie.numberInStock + rental.quantity
            rental.save()
            movie.save()
            messages.success(request, "Transaction completed successfully. The amount to be paid is {} GHS".format(
                str(rental_amount)))
            return redirect("rentedmovies")
    except Exception as ex:
        messages.error(
            request, "Unable to complete transaction. Please try again")
        capture_message(ex, level='error')
        return redirect("rentedmovies")


def get_rating_value(rating):
    return dict(RATINGS).get(rating)


def get_number_of_days(rentalDate, returnDate):
    rentalDate = datetime.strptime(str(rentalDate), "%Y-%m-%d")
    returnDate = datetime.strptime(str(returnDate), "%Y-%m-%d")
    return abs((returnDate - rentalDate).days)
