from django.core.mail import EmailMultiAlternatives
from sentry_sdk import capture_message
from django.conf import settings


class SendMail:
    def send(self, subject, to, content):
        try:
            msg = EmailMultiAlternatives(
                subject, content, settings.EMAIL_FROM, [to])
            msg.content_subtype = 'html'
            msg.send()
            return True
        except Exception as ex:
            capture_message(ex, level='error')
            return False
