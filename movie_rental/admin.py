from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Client, Movie, UserProfile

from .forms import CustomUserChangeForm, CustomUserCreationForm
from .models import CustomUser

# Register your models here.

class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser

    list_display = ('first_name', 'last_name')
    list_filter = ('email', 'is_staff', 'is_active',)
    fieldsets = (
        (None, {
            "fields": ('email', 'password', ('first_name', 'last_name')),
        }),
        ('Permissions', {
            "fields": (
                'is_staff',
                'is_active'
            ),
        }),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide'),
            "fields": (
                'email', ('password1', 'password2'), ('first_name', 'last_name'), ('is_staff', 'is_active')
            ),
        }),
    )

    search_fields = ('email',)
    ordering = ('email',)

class UserProfileAdmin(admin.ModelAdmin):
    pass

class ClientAdmin(admin.ModelAdmin):
    exclude = ('user',)

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super().save_model(request, obj, form, change)


class MovieAdmin(admin.ModelAdmin):
    exclude = ('user',)

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super().save_model(request, obj, form, change)


admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Movie, MovieAdmin)