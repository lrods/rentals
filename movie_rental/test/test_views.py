from django.test import TestCase
from django.test import Client as Req
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from ..models import Login_Log, CustomUser, Client, Movie, Rentals, RATINGS
from ..views import get_rating_value, get_number_of_days
from datetime import datetime
from django.utils import timezone
from django.contrib.auth.password_validation import ValidationError, validate_password
from ..sendMail import SendMail

sendmail = SendMail()
client = Req()

class TestLogin(TestCase):
    def test_login_view(self):
        url = reverse('login')
        response = client.get(url)
        self.assertEqual(response.status_code, 200)


class TestSignOut(TestCase):
    def setUp(self):
        CustomUser.objects.create_user(
            email='foo@foo.com', password='testdb123', first_name='John', last_name='Doe')

    def test_signout_view(self):
        url = reverse('signout')
        client.logout()
        response = client.get(url)
        self.assertRedirects(response, '/', status_code=302)

    def test_signout_date(self):
        pass
        #user = CustomUser.objects.get(email='foo@foo.com')
        #last_login = Login_Log.objects.filter(shop_attendant_id=user.id).latest('id')
        #self.assertIn(last_login, 'login_date')


class TestSignIn(TestCase):
    def setUp(self):
        CustomUser.objects.create_user(
            email='foo@foo.com', password='testdb123', first_name='John', last_name='Doe')

    def test_signin_page_loading(self):
        url = reverse('signin')
        response = client.get(url)
        self.assertRedirects(response, '/', status_code=302)

    def test_signin_incorrect_credentials(self):
        url = reverse('signin')
        response = client.post(
            url, {'email': 'foo@foo.com', 'password': '123456'})
        self.assertRedirects(response, '/', status_code=302)


class TestSignInCorrectCredentials(TestCase):
    def setUp(self):
        CustomUser.objects.create_user(
            email='foo@foo.com', password='testdb123', first_name='John', last_name='Doe')

    def test_first_login(self):
        user = authenticate(email='foo@foo.com', password='testdb123')
        self.assertTrue(isinstance(user, CustomUser))

        if user.last_login is None:
            url = reverse('signin')
            response = client.post(
                url, {'email': 'foo@foo.com', 'password': 'testdb123'})
            self.assertRedirects(
                response, '/newpassword/?email=foo@foo.com', status_code=302)

    def test_login_active_user(self):
        url = reverse('signin')
        c_user = CustomUser.objects.get(email='foo@foo.com')
        c_user.last_login = timezone.now()
        c_user.save()
        response = client.post(
            url, {'email': 'foo@foo.com', 'password': 'testdb123'})

        login = client.login(email='foo@foo.com', password='testdb123')
        self.assertTrue(login)
        self.assertRedirects(response, '/home', status_code=302)

    def test_login_inactive_user(self):
        url = reverse('signin')
        c_user = CustomUser.objects.get(email='foo@foo.com')
        c_user.last_login = timezone.now()
        c_user.is_active = False
        c_user.save()
        response = client.post(
            url, {'email': 'foo@foo.com', 'password': 'testdb123'})

        self.assertRedirects(response, '/', status_code=302)

        #url = reverse('signin')
        #response = client.post(url, {'email': 'demo.apps19@gmail.com', 'password': 'movie123'})
        # self.assertEqual(response.url, '/home', ?next=/home)


class TestNewPassword(TestCase):
    def setUp(self):
        CustomUser.objects.create_user(
            email='foo@foo.com', password='testdb123', first_name='John', last_name='Doe')

    def test_no_mail_provided(self):
        url = reverse('newpassword')
        response = client.post(url, {'': '', 'password': 'maria123t'})
        self.assertRedirects(response, '/', status_code=302)

    def test_wrong_mail_provided(self):
        url = reverse('newpassword')
        response = client.post(
            url, {'mail': 'foo@gmail.com', 'password': 'maria123t'})
        self.assertEqual(response.status_code, 404)

    def test_same_password_provided(self):
        url = reverse('newpassword')
        response = client.post(url, {'mail': 'foo@foo.com', 'password': 'testdb123'})
        self.assertRedirects(response, '/newpassword/?email=foo@foo.com', status_code=302)

    def test_different_password_provided(self):
        url = reverse('newpassword')
        response = client.post(
            url, {'mail': 'foo@foo.com', 'password': 'maria123t'})
        self.assertRedirects(response, '/home', status_code=302)

    def test_exception_raised(self):
        with self.assertRaises(ValidationError):
            validate_password('1234')

class TestUtilityFunction(TestCase):
    def test_rating_value(self):
        rating = get_rating_value(1)
        self.assertEqual(rating, 'PG')

    def test_number_of_days(self):
        nod = get_number_of_days(datetime.now().date(), datetime.now().date())
        self.assertEqual(nod, 0)


class TestHomepage(TestCase):
    def setUp(self):
        CustomUser.objects.create_user(
            email='foo@foo.com', password='testdb123', first_name='John', last_name='Doe')

    def test_return_to_login_page(self):
        url = reverse('home')
        response = client.get(url)
        self.assertEqual(response.status_code, 302)

    def test_homepage_loaded(self):
        client.login(email='foo@foo.com', password='testdb123')
        url = reverse('home')
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'movie_rental/index.html')


class TestResetPassword(TestCase):
    def test_password_reset_page(self):
        url = reverse('forgot')
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'movie_rental/resetpassword.html')

    def test_sending_mail(self):
        url = reverse('forgot')
        response = client.post(url, {'email': 'demo.apps19@gmail.com'})
        self.assertRedirects(response, '/forgot', status_code=302)


class TestClientView(TestCase):
    def setUp(self):
        CustomUser.objects.create_user(
            email='foo@foo.com', password='testdb123', first_name='John', last_name='Doe')

        Client.objects.create(first_name='Jane', last_name='Doe', email_address='jane.doe@mail.com',
                              address='20 Beach Walk Avenue', telephone_number='0204598430',
                              date_of_birth=datetime(1990, 5, 17).date(), user=CustomUser.objects.get(email='foo@foo.com'))

    def test_client_page_no_login(self):
        url = reverse('client')
        response = client.get(url)
        self.assertRedirects(response, '/?next=/client', status_code=302)

    def test_client_page_loading(self):
        client.login(email='foo@foo.com', password='testdb123')
        url = reverse('client')
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'movie_rental/clients.html')

    def test_save_client_data_incorrect(self):
        client.login(email='foo@foo.com', password='testdb123')
        url = reverse('client')
        response = client.post(url, {'first_name': '', 'last_name': '', 'email_address': '', 'address': '',
                                     'telephone_number': '', 'date_of_birth': '', 'membership_type': ''})
        self.assertRedirects(response, '/add', status_code=302)

    def test_save_client_data_correct(self):
        client.login(email='foo@foo.com', password='testdb123')
        url = reverse('client')
        response = client.post(url, {'fname': 'Jane', 'lname': 'Doe', 'email_address': 'mail1@mail.com', 'address': '20, Long View Drive',
                                     'tel_number': '0209854982', 'date_of_birth': datetime(1990, 4, 25).date(), 'member_type': '2',
                                     'user': CustomUser.objects.get(email='foo@foo.com')})
        self.assertRedirects(response, '/client', status_code=302)

    def test_modify_not_existing_client_(self):
        client.login(email='foo@foo.com', password='testdb123')
        url = reverse('client')
        response = client.post(url, {'fname': 'Jane', 'lname': 'Doe', 'email_address': 'mail@mail.com', 'address': '20, Long View Drive',
                                     'tel_number': '0209854982', 'date_of_birth': datetime(1990, 4, 25).date(), 'member_type': '2',
                                     'user': CustomUser.objects.get(email='foo@foo.com'), 'client_id': 2})
        self.assertEqual(response.status_code, 404)

    def test_modify_existing_client_correct_data(self):
        client.login(email='foo@foo.com', password='testdb123')
        url = reverse('client')
        customer = Client.objects.get(email_address='jane.doe@mail.com')
        response = client.post(url, {'email_address': 'jane_doe@mail.com', 'address': '20, Long View Drive', 'tel_number': '0209854982',
                                     'member_type': '2', 'client_id': customer.id})
        self.assertRedirects(response, '/client', status_code=302)


class TestUpdateClient(TestCase):
    def setUp(self):
        CustomUser.objects.create_user(
            email='foo@foo.com', password='testdb123', first_name='John', last_name='Doe')

        Client.objects.create(first_name='Jane', last_name='Doe', email_address='jane.doe@mail.com',
                              address='20 Beach Walk Avenue', telephone_number='0204598430',
                              date_of_birth=datetime(1990, 5, 17).date(), user=CustomUser.objects.get(email='foo@foo.com'))

    def test_update_client_page_no_login(self):
        response = client.get('/change/1')
        self.assertRedirects(response, '/?next=/change/1', status_code=302)

    def test_update_client_page_login_incorrect_id(self):
        client.login(email='foo@foo.com', password='testdb123')
        response = client.get('change/', {'id': 1})
        self.assertEqual(response.status_code, 404)

    def test_update_client_page_login_correct_id(self):
        client.login(email='foo@foo.com', password='testdb123')
        customer = Client.objects.get(email_address='jane.doe@mail.com')
        response = client.get('/change/' + str(customer.id))
        self.assertEqual(response.status_code, 200)


class TestClientDetails(TestCase):
    def setUp(self):
        CustomUser.objects.create_user(
            email='foo@foo.com', password='testdb123', first_name='John', last_name='Doe')

        Client.objects.create(first_name='Jane', last_name='Doe', email_address='jane.doe@mail.com',
                              address='20 Beach Walk Avenue', telephone_number='0204598430',
                              date_of_birth=datetime(1990, 5, 17).date(), user=CustomUser.objects.get(email='foo@foo.com'))

    def test_client_details_no_client(self):
        response = client.get('/client/1')
        self.assertEqual(response.status_code, 404)

    def test_client_details_existing_client(self):
        customer = Client.objects.get(email_address='jane.doe@mail.com')
        response = client.get('/client/' + str(customer.id))
        self.assertJSONEqual(str(response.content, encoding='utf8'), {'first_name': 'Jane', 'last_name': 'Doe', 'email_address': 'jane.doe@mail.com',
                                                                      'address': '20 Beach Walk Avenue', 'telephone_number': '0204598430',
                                                                      'date_of_birth': '1990-05-17', 'membership_type': 1})


class TestMovie(TestCase):
    def setUp(self):
        CustomUser.objects.create_user(
            email='foo@foo.com', password='testdb123', first_name='John', last_name='Doe')

        Movie.objects.create(title='Django Unchained', director='Quentin Tarantino', rating=5,
                             dailyRentalRate=3.5, numberInStock=100, user=CustomUser.objects.get(email='foo@foo.com'))

        Client.objects.create(first_name='Jane', last_name='Doe', email_address='jane.doe@mail.com',
                              address='20 Beach Walk Avenue', telephone_number='0204598430',
                              date_of_birth=datetime(1990, 5, 17).date(), user=CustomUser.objects.get(email='foo@foo.com'))

    def test_movie_page_redirect(self):
        url = reverse('movies')
        response = client.get(url)
        self.assertRedirects(response, '/?next=/movies', status_code=302)

    def test_movie_page_loading(self):
        client.login(email='foo@foo.com', password='testdb123')
        url = reverse('movies')
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'movie_rental/movies.html')

    def test_rent_movie_wrong_id(self):
        client.login(email='foo@foo.com', password='testdb123')
        response = client.get('/rentmovie/1')
        self.assertEqual(response.status_code, 404)

    def test_rent_movie_loading(self):
        client.login(email='foo@foo.com', password='testdb123')
        movie = Movie.objects.get(title='Django Unchained')
        response = client.get('/rentmovie/' + str(movie.id))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'movie_rental/rentmovie.html')

    def test_rental_save_incorrect_client(self):
        client.login(email='foo@foo.com', password='testdb123')
        movie = Movie.objects.get(title='Django Unchained')
        response = client.post(
            '/rentmovie/' + str(movie.id), {'client': 1, 'quantity': 2})
        self.assertEqual(response.status_code, 404)

    def test_rental_save_correct_client(self):
        client.login(email='foo@foo.com', password='testdb123')
        movie = Movie.objects.get(title='Django Unchained')
        customer = Client.objects.get(email_address='jane.doe@mail.com')
        response = client.post('/rentmovie/' + str(movie.id), {'movie': movie, 'client': customer.id, 'quantity': 2,
                                                               'shop_attendant': CustomUser.objects.get(email='foo@foo.com')})
        self.assertRedirects(response, '/movies', status_code=302)


class TestReturnMovie(TestCase):
    def setUp(self):
        CustomUser.objects.create_user(
            email='foo@foo.com', password='testdb123', first_name='John', last_name='Doe')

        Movie.objects.create(title='Django Unchained', director='Quentin Tarantino', rating=5,
                             dailyRentalRate=3.5, numberInStock=100, user=CustomUser.objects.get(email='foo@foo.com'))

        Client.objects.create(first_name='Jane', last_name='Doe', email_address='jane.doe@mail.com',
                              address='20 Beach Walk Avenue', telephone_number='0204598430',
                              date_of_birth=datetime(1990, 5, 17).date(), user=CustomUser.objects.get(email='foo@foo.com'))

        Rentals.objects.create(movie=Movie.objects.get(title='Django Unchained'), client=Client.objects.get(email_address='jane.doe@mail.com'),
                               quantity=2, shop_attendant=CustomUser.objects.get(email='foo@foo.com'))

    def test_rented_movies_page_redirect(self):
        url = reverse('rentedmovies')
        response = client.get(url)
        self.assertRedirects(response, '/?next=/return', status_code=302)

    def test_rented_movies_page_loading(self):
        client.login(email='foo@foo.com', password='testdb123')
        url = reverse('rentedmovies')
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'movie_rental/returns.html')
        
    def test_return_page_redirect(self):
        response = client.get('/return_movie/1/1')
        self.assertRedirects(response, '/?next=/return_movie/1/1', status_code=302)

    def test_return_page_wrong_id(self):
        client.login(email='foo@foo.com', password='testdb123')
        response = client.post('/return_movie/1/1')
        self.assertEqual(response.status_code, 404)

    def test_rent_movie_correct_id(self):
        movie = Movie.objects.get(title='Django Unchained')
        rental = Rentals.objects.get(movie_id=movie.id)
        client.login(email='foo@foo.com', password='testdb123')
        response = client.post('/return_movie/' + str(rental.id) + '/' + str(movie.id))
        self.assertRedirects(response, '/return', status_code=302)