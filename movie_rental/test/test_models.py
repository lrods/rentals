from django.test import TestCase
from ..models import CustomUser, Client, Movie, Rentals
from datetime import datetime


class CustomUserTest(TestCase):
    def create_customUser(self, email, password, first_name, last_name):
        return CustomUser.objects.create_user(email=email, password=password, first_name=first_name, last_name=last_name)

    def test_customUser_creation(self):
        user = self.create_customUser('foo@foo.com', 'test678', 'John', 'Doe')
        self.assertTrue(isinstance(user, CustomUser))
        self.assertEqual(str(user), user.first_name)

class ClientTest(TestCase):
    def create_client(self, first_name, last_name, email_address, address, telephone_number, date_of_birth, user):
        return Client.objects.create(first_name=first_name, last_name=last_name, email_address=email_address,
        address=address, telephone_number=telephone_number, date_of_birth=date_of_birth, user=user)

    def test_client_creation(self):
        user = CustomUserTest().create_customUser('foo@foo.com', 'test678', 'John', 'Doe')
        client = self.create_client('Jane', 'Doe', 'jane.doe@mail.com', '20 Beach Walk Avenue', 
        '0204598430', datetime(1990, 5, 17), user=user)
        self.assertTrue(isinstance(client, Client))
        self.assertEqual(str(client), client.first_name + " " + client.last_name)

class MovieTest(TestCase):
    def create_movie(self, title, director, rating, dailyRentalRate, numberInStock, user):
        return Movie.objects.create(title=title, director=director, rating=rating, 
        dailyRentalRate=dailyRentalRate, numberInStock=numberInStock, user=user)
    
    def test_movie_creation(self):
        user = CustomUserTest().create_customUser('foo@foo.com', 'test678', 'John', 'Doe')
        movie = self.create_movie('Django Unchained', 'Quentin Tarantino', 5, 3.5, 100, user)
        self.assertTrue(isinstance(movie, Movie))
        self.assertEqual(str(movie), movie.title)

class RentalTest(TestCase):
    def create_rental(self, movie, client, quantity, user):
        return Rentals.objects.create(movie=movie, client=client, quantity=quantity, shop_attendant=user)

    def test_rental_creation(self):
        user = CustomUserTest().create_customUser('foo@foo.com', 'test678', 'John', 'Doe')
        client = ClientTest().create_client('Jane', 'Doe', 'jane.doe@mail.com', '20 Beach Walk Avenue', 
        '0204598430', datetime(1990, 5, 17), user=user)
        movie = MovieTest().create_movie('Django Unchained', 'Quentin Tarantino', 5, 3.5, 100, user)
        rental = self.create_rental(movie, client, 2, user)
        self.assertTrue(isinstance(rental, Rentals))
        self.assertEqual(int(str(rental)), rental.id)

#class EmailTest(TestCase):
