from .models import Client, Movie, Rentals
from .serializers import ClientSerializer, MovieSerializer, RentalSerializer
from rest_framework import viewsets, filters

class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    #filter_backend = [filters.SearchFilter]
    #search_fields = ['first_name', 'last_name']

class MovieViewset(viewsets.ModelViewSet):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer

class RentalViewSet(viewsets.ModelViewSet):
    queryset = Rentals.objects.filter(status=1)
    serializer_class = RentalSerializer