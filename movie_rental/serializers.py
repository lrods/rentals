from rest_framework import serializers
from .models import Client, Movie, Rentals

class ClientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'first_name', 'last_name', 'email_address', 'membership_type']

class ClientDetailsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Client
        fields = ['first_name', 'last_name', 'email_address', 'address', 'telephone_number', 
        'date_of_birth', 'membership_type']

class MovieSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Movie
        fields = ['id', 'title', 'rating', 'genre']

class MovieRentalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = ['id', 'title']

class ClientRentalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'first_name', 'last_name']

class RentalSerializer(serializers.ModelSerializer):
    movie = MovieRentalSerializer()
    client = ClientRentalSerializer()

    class Meta:
        model = Rentals
        fields = ['id', 'movie', 'client', 'quantity', 'status', 'date_rented']
        depth = 1