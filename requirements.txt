Django==3.0.3
djangorestframework==3.10.3
gunicorn==20.0.4
mysqlclient==1.4.4
PyMySQL==0.9.2
whitenoise==5.0.1
sentry-sdk==0.14.3
django-mailgun==0.9.1
